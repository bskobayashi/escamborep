# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts "Cadastrando Categorias..."

categoies = [ "Animais", 
							"Esportes", 
							"Para a casa", 
							"Eletronicos e Celulares", 
							"Música e Hobbies", 
							"Infantil", 
							"Moda e Beleza", 
							"Veiculos", 
							"Imóveis", 
							"Empregos" ]

categoies.each do |category|
	Category.find_or_create_by(description: category)
	puts "Categoria #{category} cadastrada com sucesso!!!"
end

#########################################################

puts "Cadastrando Administrador padrão..."

Admin.create!(name: "Administrador", 
              email: "admin@admin.com", 
              password: "123456", 
              password_confirmation: "123456",
              role: 0
              )
puts "Administrador #{Admin.name} cadastrado com sucesso!!!"